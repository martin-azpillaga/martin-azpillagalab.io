export class MiniElement extends HTMLElement
{
	constructor ()
	{
		super();

		this.root = this.attachShadow({mode: "open"});

		this.imageElements = [];
		for (let i = 0; i < this.images; i++)
		{
			this.imageElements.push(document.createElement("img"));
		}

		this.paragraphElements = [];
		for (let i = 0; i < this.paragraphs; i++)
		{
			this.paragraphElements.push(document.createElement("p"));
		}

		this.styler = document.createElement("style");
		this.styler.textContent = this.styles;

		this.afterConstruction?.()
	}

	attributeChangedCallback (attribute, old, value)
	{
		const attributes = this.constructor.observedAttributes;

		for (let i = 0; i < this.images; i++)
		{
			if (attribute === attributes[i])
			{
				this.imageElements[i].src = value;
				this.imageElements[i].alt = value;
			}
		}

		for (let i = 0; i < this.paragraphs; i++)
		{
			if (attribute === attributes[i + this.images])
			{
				this.paragraphElements[i].textContent = value;
			}
		}
	}

	connectedCallback ()
	{
		for (let i = 0; i < this.images; i++)
		{
			this.root.append(this.imageElements[i]);
		}

		for (let i = 0; i < this.paragraphs; i++)
		{
			this.root.append(this.paragraphElements[i]);
		}

		while (this.firstChild)
		{
			this.root.append(this.firstChild);
		}

		this.root.append(this.styler);
	}
}

window.definedElements = {};
const old = customElements.define.bind(customElements);

customElements.define = (a,b) =>
{
	definedElements[a.toUpperCase()] = b.observedAttributes;
	old(a,b);
}

export class Card extends MiniElement
{
	get images () { return 1; }
	get paragraphs () { return 3; }
	get styles () { return `
:host
{
	display: grid;
	grid: 'picture headline' 'picture title' 'picture subtitle' 'content content' / fit-content(10px) auto;
	grid-gap: 0 1.5em;
	padding: 1em;
}
*
{
	grid-column: 1 / 3;
	margin: 1em 0;
}
img
{
	height: 4.6em;
	width: 4.6em;
	object-fit: contain;
	border-radius: 10%;
	grid-area: picture;
}
p:nth-of-type(1)
{
	margin: 0;
	font-size: larger;
	font-weight: bolder;
	grid-area: headline;
}
p:nth-of-type(2)
{
	margin: 0.5em 0 0 0;
	grid-area: title;
}
p:nth-of-type(3)
{
	margin: 0.5em 0 0 0;
	grid-area: subtitle;
}
p
{
	max-width: 45ch;
}
a
{
	color: white;
	letter-spacing: 1.3;
}`;}
}

export class Set extends MiniElement
{
	get images () { return 0; }
	get paragraphs () { return 1; }
	get styles () { return `
:host
{
	display: grid;
	place-items: center start;
	padding: 1em 0;
	width: 100%;
	grid-auto-columns: minmax(0, 1fr);
}
*
{
	grid-row: 2;
	font-size: smaller;
}
p:nth-of-type(1)
{
	margin: 0;
	font-weight: bolder;
	grid-row: 1;
	grid-column-start: 1;
	place-self: center center;
}`;}

	afterConstruction ()
	{
		this.paragraphElements[0].style.gridColumnEnd = this.childElementCount + 1;
	}
}

export class One extends MiniElement
{
	get images () { return 1; }
	get paragraphs () { return 1; }
	get styles () { return `
:host
{
	display: grid;
	grid-auto-flow: column;
	place-items: center start;
	padding: 1em;
	gap: 1em;
	grid-template-columns: min-content;
}
img
{
	width: 2em;
	height: 2em;
	object-fit: contain;
}
p
{
	text-align: justify;
}`}
}

export class Bio extends MiniElement
{
	get images () { return 1; }
	get paragraphs () { return 1; }
	get styles () { return `
:host
{
	padding: 0 1em 1em 1em;
	display: grid;
	grid-auto-rows: min-content;
}
img
{
	border-radius: 50%;
	width: 80%;
	place-self: center;
}
p:nth-of-type(1)
{
	grid-row: 1;
	font-size: larger;
	font-weight: bolder;
}`}
}

export class TitledList extends MiniElement
{
	get images () { return 0; }
	get paragraphs () { return 1; }
	get styles () { return `
:host
{
	display: grid;
	grid-auto-rows: min-content;
	padding: 1em;
}
p
{
	font-size: larger;
	font-weight: bolder;
}`;}
}

export class Translations extends HTMLElement
{
	constructor ()
	{
		super();

		this.root = this.attachShadow({mode: "closed"});

		this.select = document.createElement("select");

		this.styler = document.createElement("style");

		this.languages = [];

		this.last_selected = 0;
		this.selected = 0;

		this.styler.textContent =
		`
		select
		{
			padding: 1em;
			width: 100%;
			text-align: center;
		}
		`;

		this.select.onchange = () =>
		{
			this.last_selected = this.selected;
			this.selected = this.languages.indexOf(this.select.value);

			for (const element of document.body.getElementsByTagName("*"))
			{
				this.translate(element);
			}
		}
	}

	static get observedAttributes ()
	{
		return ["csv"];
	}

	async attributeChangedCallback (attribute, old, value)
	{
		if (attribute === "csv")
		{
			const response = await fetch(value);
			const csv = await response.text();
			const [title, ...rows] = csv.split("\n").filter(x => x !== "");

			this.languages = title.split(", ");
			this.rows = rows.map(x => x.split(", "));

			for (const language of this.languages)
			{
				const option = document.createElement("option");
				option.textContent = language;
				this.select.append(option);
			}
		}
	}

	translate (element)
	{
		console.log(element);
		if (definedElements[element.tagName])
		{
			for (const attribute of definedElements[element.tagName])
			{
				for (const row of this.rows)
				{
					if (row[this.last_selected] === element.getAttribute(attribute))
					{
						element.setAttribute(attribute, row[this.selected]);
					}
				}
			}
		}
		for (const row of this.rows)
		{
			if (row[this.last_selected] === element.textContent)
			{
				element.textContent = row[this.selected];
			}
		}

		if (element.shadowRoot)
		{
			for (const e of element.shadowRoot.querySelectorAll("*"))
			{
				this.translate(e);
			}
		}
	}

	connectedCallback ()
	{
		this.root.append(this.select, this.styler);
	}
}

const components =
[
	["cv-section", TitledList, ["headline"]],
	["cv-job", Card, ["logo", "position", "company", "date"]],
	["cv-diploma", Card, ["logo", "field", "school", "date"]],
	["cv-project", Card, ["logo", "name", "description", "date"]],
	["cv-language", One, ["flag", "description"]],
	["cv-skill-set", Set, ["headline"]],
	["cv-skill", One, ["picture", "description"]],
	["cv-open-source", One, ["picture", "description"]],
	["cv-contact", One, ["picture", "description"]],
	["cv-bio", Bio, ["picture", "headline"]],
	["cv-translator", Translations, ["csv"]]
];

for (const [name, superclass, attributes] of components)
{
	customElements.define(name, class extends superclass
	{
		static get observedAttributes () { return attributes; }
	});
}
